# Expense Manager
# Author: Darren Mistry

Features:

1. Dynamic number of categories
2. Graphical and tabular analysis
3. Analysis by day, range of days or month
4. Quick analysis mode (Previous 24 hrs,week, month, year)
5. Signup and use from anywhere and from any device
6. Can change password/email or delete your account
7. Animated charts
8. Append or replace any day's cost
9. Attractive user interface and friendly user experience